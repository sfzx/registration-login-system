/* eslint-env jest */
const app = require('../app')
const request = require('supertest')
const authService = require('../app/auth/auth.service')
const mongoose = require('mongoose')

const newUser = {
  name: 'name',
  email: 'new@email.com',
  password: 'asD1234'
}

describe('/', () => {
  afterAll(async () => {
    mongoose.connection.close()
  })

  beforeEach(async () => {
    await authService.saveData(newUser)
  })

  afterEach(async () => {
    await authService.deleteUser(newUser)
  })

  test('On POST /register should create user', async () => {
    const res = await request(app)
      .post('/register')
      .send({
        name: 'name2',
        email: 'new2@email.com',
        password: 'asD12341'
      })
    await authService.deleteUser({ email: 'new2@email.com' })
    expect(res.statusCode).toBe(200)
    expect(res.text).toBe('User successfully created')
  })

  test('On POST /register should return error if user with such email exist', async () => {
    const res = await request(app)
      .post('/register')
      .send({
        name: 'name',
        email: 'new@email.com',
        password: 'asD12341'
      })
    expect(res.statusCode).toBe(400)
    expect(res.body.message).toBe('Email already exists')
  })

  test('On POST /register should return error if invalid input', async () => {
    const res = await request(app)
      .post('/register')
      .send({
        name: 'name',
        emal: 'new2@email.com',
        password: 'asD12341'
      })
    expect(res.statusCode).toBe(400)
    expect(res.body.message).toBe('"email" is required')
  })

  test('On POST /login should login user', async () => {
    const res = await request(app)
      .post('/login')
      .send({
        email: 'new@email.com',
        password: 'asD1234'
      })
    expect(res.statusCode).toBe(302)
  })

  test('On POST /login should return error if invalid input', async () => {
    const res = await request(app)
      .post('/login')
      .send({
        email: 'new@email.ru',
        password: 'asD1234'
      })
    expect(res.statusCode).toBe(400)
    expect(res.body.message).toBe('"email" must be a valid email')
  })

  test('On POST /login should return error if no user found', async () => {
    const res = await request(app)
      .post('/login')
      .send({
        email: 'new2@email.com',
        password: 'asD1234'
      })
    expect(res.statusCode).toBe(302)
    expect(res.text).toBe('Found. Redirecting to /login')
  })
})
