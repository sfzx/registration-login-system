const express = require('express')
const router = express.Router()
const passport = require('passport')
const validation = require('./auth.validation')
const authService = require('./auth.service')

router.get('/', validation.checkAuthenticated, (req, res) => {
  res.render('index.ejs', { name: req.user.name })
})

router.get('/login', validation.checkNotAuthenticated, (req, res) => {
  res.render('login.ejs')
})

router.post('/login', validation.loginBody, passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
  failureFlash: true
})
)

router.get('/register', validation.checkNotAuthenticated, (req, res) => {
  res.render('register.ejs')
})

router.post('/register', validation.checkNotAuthenticated, validation.registerBody, async (req, res) => {
  await authService.saveData(req.body)
  res.status(200).send('User successfully created')
})

router.delete('/logout', (req, res) => {
  req.logOut()
  res.redirect('/login')
})

module.exports = router
