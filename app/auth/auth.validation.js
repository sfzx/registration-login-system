const Joi = require('@hapi/joi')
const authService = require('./auth.service')
const { ErrorHandler } = require('../helpers/error')

const registerSchema = Joi.object({
  name: Joi.string().alphanum().min(1).required(),
  email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'ua'] } }).required(),
  password: Joi.string().min(6).max(30).required()
}).required()

const loginSchema = Joi.object({
  email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'ua'] } }),
  password: Joi.string().min(6).max(30)
})

async function bodyValidation (body, schema, next) {
  try {
    const result = await schema.validate(body)
    if (result.error) {
      throw new ErrorHandler(400, result.error.details[0].message)
    }
  } catch (error) {
    next(error)
  }
}

async function checkExistence (email, next) {
  try {
    if (await authService.findUser({ email })) {
      throw new ErrorHandler(400, 'Email already exists')
    }
  } catch (error) {
    next(error)
  }
}

const validation = {
  checkAuthenticated: (req, res, next) => {
    if (req.isAuthenticated()) {
      next()
    } else {
      res.redirect('/login')
    }
  },

  checkNotAuthenticated: (req, res, next) => {
    if (req.isAuthenticated()) {
      res.redirect('/')
    } else {
      next()
    }
  },

  registerBody: async (req, res, next) => {
    await bodyValidation(req.body, registerSchema, next)
    await checkExistence(req.body.email, next)
    await next()
  },

  loginBody: async (req, res, next) => {
    await bodyValidation(req.body, loginSchema, next)
    await next()
  }
}

module.exports = validation
