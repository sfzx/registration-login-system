const User = require('./auth.user.model')
const bcrypt = require('bcrypt')

const authService = {
  findUser: async (data) => {
    return await User.findOne(data)
  },
  saveData: async (data) => {
    data.password = await bcrypt.hash(data.password, 10)
    const user = new User(data)
    return await user.save()
  },
  deleteUser: async (data) => {
    return await User.deleteOne(data)
  }
}

module.exports = authService
