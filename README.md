# Registration-Login system express project
## Prerequisites
node version 12 or higher installed

## Installation
```
npm i
```

## Start
```
npm start
```

## Running tests
```
npm run test
```

## Project preview

Project deployed on [Heroku](https://registration-login-system.herokuapp.com/).

## Technologies used
```
Express, mongodb, passport, @hapi/joi, bcrypt, ejs, jest, supertest
```