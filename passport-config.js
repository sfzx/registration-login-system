const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')
const User = require('./app/auth/auth.user.model')

function initialize (passport) {
  const authenticateUser = (email, password, done) => {
    User.findOne({ email: email }, async user => {
      if (user == null) {
        return done(null, false, { message: 'No user with that email' })
      }
      try {
        if (await bcrypt.compare(password, user.password)) {
          return done(null, user)
        } else {
          return done(null, false, { message: 'Wrong password' })
        }
      } catch (error) {
        return done(error)
      }
    })
  }

  passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser))
  passport.serializeUser((user, done) => done(null, user.id))
  passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
      done(err, user)
    })
  })
}

module.exports = initialize
